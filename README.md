[![Jeopardy!](Jeopardy!_logo.svg)](https://en.wikipedia.org/wiki/Jeopardy!)

...is an American game show created by Merv Griffin.
The show is a quiz competition that
reverses the traditional question-and-answer format of many quiz shows.
Rather than being given questions,
contestants are instead given general knowledge clues
in the form of answers and they must identify the
person, place, thing, or idea that
the clue describes, phrasing each response in the form of a question.

# ...in Rust

## Installation

Clone this repository and install the `trunk` crate for Rust.

```sh
cargo install trunk
```

Then,
inside of this repository,
run the local web server and
open the page it serves.

```sh
trunk serve --open
```

