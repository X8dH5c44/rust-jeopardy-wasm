use yew::prelude::*;
use yew_template::template_html;

use crate::services::locale;

pub struct Component;

impl yew::Component for Component {
  type Message = ();
  type Properties = ();

  fn create(_: &Context<Self>) -> Self {
    Self
  }

  fn view(&self, _: &Context<Self>) -> Html {
    let locale = locale::get();
    template_html!("html/scoreboard/component.html", ...)
  }
}

