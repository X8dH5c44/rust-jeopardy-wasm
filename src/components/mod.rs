mod greeting;
pub use greeting::Component as GreetingComponent;

mod setup;
pub use setup::Component as SetupComponent;

mod board;
pub use board::Component as BoardComponent;

mod answer;
pub use answer::Component as AnswerComponent;

mod scoreboard;
pub use scoreboard::Component as ScoreboardComponent;

