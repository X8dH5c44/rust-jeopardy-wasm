use yew::prelude::*;
use yew_template::template_html;

use crate::components::{
  GreetingComponent,
  SetupComponent,
  BoardComponent,
  AnswerComponent,
  ScoreboardComponent
};
use crate::services::locale;

#[derive(Default)]
pub struct App {
  pub view: View
}

pub enum View {
  Greeting,
  Setup,
  Board,
  Answer,
  Scoreboard
}

pub enum Message {
  ToView(View)
}

impl Default for View {
  fn default() -> Self {
      View::Greeting
  }
}

impl Component for App {
  type Message = Message;
  type Properties = ();

  fn create(_: &Context<Self>) -> Self { Self::default() }

  fn update(&mut self, _: &Context<Self>, msg: Self::Message) -> bool {
      match msg {
        Message::ToView(view) => self.view = view
      }
      true
  }

  fn view(&self, ctx: &Context<Self>) -> Html {
    let locale = locale::get();
    let view = get_view_html(&self.view);
    let to_greeting = to_greeting(ctx);
    let to_setup = to_setup(ctx);
    let to_board = to_board(ctx);
    let to_answer = to_answer(ctx);
    let to_scoreboard = to_scoreboard(ctx);
    template_html!("html/app.html", ...)
  }
}

fn get_view_html(view: &View) -> Html {
  match view {
    View::Greeting => html!(<GreetingComponent/>),
    View::Setup => html!(<SetupComponent/>),
    View::Board => html!(<BoardComponent/>),
    View::Answer => html!(<AnswerComponent/>),
    View::Scoreboard => html!(<ScoreboardComponent/>)
  }
}

fn to_greeting(ctx: &Context<App>) -> Callback<MouseEvent> {
  ctx.link().callback(|_| Message::ToView(View::Greeting))
}

fn to_setup(ctx: &Context<App>) -> Callback<MouseEvent> {
  ctx.link().callback(|_| Message::ToView(View::Setup))
}

fn to_board(ctx: &Context<App>) -> Callback<MouseEvent> {
  ctx.link().callback(|_| Message::ToView(View::Board))
}

fn to_answer(ctx: &Context<App>) -> Callback<MouseEvent> {
  ctx.link().callback(|_| Message::ToView(View::Answer))
}

fn to_scoreboard(ctx: &Context<App>) -> Callback<MouseEvent> {
  ctx.link().callback(|_| Message::ToView(View::Scoreboard))
}
