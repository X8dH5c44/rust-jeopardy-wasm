# Overview
The game has the following states with their respective transitions:
* [Greeting](01-greeting.md)
  * Setup
* [Setup](02-setup.md)
  * Board
* [Board](03-board.md)
  * Answer
  * Scoreboard
* [Answer](04-answer.md)
  * Board
* [Scoreboard](05-scoreboard.md)

